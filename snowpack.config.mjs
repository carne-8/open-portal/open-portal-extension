export default {
  mount: {
    public: { url: "/", static: true },
    popup: { url: "/popup" }
  },
  plugins: [
    "@snowpack/plugin-sass",
    "@snowpack/plugin-webpack",
    "@snowpack/plugin-react-refresh"
  ],
  packageOptions: {
    polyfillNode: true,
  },
  devOptions: {
    // open: "none",
    port: 8080,
  },
}